@extends('layouts.app')

@section('content')
<div class="container">


@if(Session::has('mensaje'))
<div class="alert alert-success" role="alert">
{{ Session::get('mensaje')}}
</div>
@endif


<a href="{{ url('usuario/create') }}" class="btn btn-outline-info"> Quieres volver a registrarte ?</a>
<br>
<br>
<table class="table table-striped">

    <thead class="thead-dark">
        <tr>
            <td>#</td>
            <th> Foto </th>
            <th> Nombre </th>
            <th> Apellido </th>
            <th> Correo </th>
            <th> Botones </th>
        </tr>
    </thead>

    <tbody>
    @foreach($usuario as $usuarionuevo)
        <tr>
            <td>{{$usuarionuevo->id}}</td>

            <td>
            <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$usuarionuevo->Foto }}" width="100" alt="">
            </td>

            <td>{{$usuarionuevo->id }}</td>
            <td>{{$usuarionuevo->Nombre }}</td>
            <td>{{$usuarionuevo->Apellido }}</td>
            <td>{{$usuarionuevo->Correo }}</td>
            <td>

        <a href="{{ url('/usuario/'.$usuarionuevo->id.'/edit') }}" class="btn btn-outline-success">
        Editar
        </a>

            <form method="post" class="d-inline" action="{{ url('/usuario/'.$usuarionuevo->id )}}">
                @csrf
                {{ method_field('DELETE') }}
                <input class="btn btn-danger" type="submit" onclick="return confirm('Quieres borrar? seguro ?')"
                value="Borrar">



            </form>
            
             </td>
        </tr>
        @endforeach
    </tbody>
</table>
{!! $usuario->links()!!}
</div>
@endsection