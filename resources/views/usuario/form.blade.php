
<h1>FORMULARIO USUARIO</h1>

@if(count($errors)>0)

<div class="alert alert-danger" role="alert">
<ul>


@foreach( $errors->all() as $errors)
<li>{{ $errors}}</li>
@endforeach
</ul>
<div>

@endif
<label for="Nombre"> Nombre</label>
<input type="text" name="Nombre" class="form-control is-valid" value="{{ isset($usuario->Nombre)?$usuario->Nombre:old('Nombre') }}" id="Nombre">


<label for="Apellido"> Apellido</label>
<input type="text" name="Apellido" class="form-control is-valid" value="{{ isset($usuario->Apellido)?$usuario->Apellido:old('Apellido') }}" id="Apellido">


<label for="Correo"> Correo</label>
<input type="text" name="Correo" class="form-control is-valid"value="{{ isset($usuario->Correo)?$usuario->Correo:old('Correo') }}" id="Correo">

<br>
<label for="Foto"></label>
@if(isset($usuario->Foto))
<img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$usuario->Foto }}" width="100" alt="">
@endif
<input type="file" class="form-control" name="Foto" value="" id="Foto">

<input class="form-control" type="submit" class="btn btn-primary" value="{{$modo}} datos">

<a class="btn btn-outline-info" href="{{ url('usuario/') }}"> Regresa a tu listado </a>

