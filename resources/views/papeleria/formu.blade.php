
<h1>FORMULARIO DE PAPELERIAS</h1>

@if(count($errors)>0)

<div class="alert alert-danger" role="alert">
<ul>


@foreach( $errors->all() as $errors)
<li>{{ $errors}}</li>
@endforeach
</ul>
<div>

@endif
<label for="Nombre"> Nombre</label>
<input type="text" name="Nombre" class="form-control is-valid" value="{{ isset($papeleria->Nombre)?$papeleria->Nombre:old('Nombre') }}" id="Nombre">


<label for="Telefono"> Telefono</label>
<input type="text" name="Telefono" class="form-control is-valid" value="{{ isset($papeleria->Telefono)?$papeleria->Telefono:old('Telefono') }}" id="Telefono">


<label for="Calle"> Calle</label>
<input type="text" name="Calle" class="form-control is-valid"value="{{ isset($papeleria->Calle)?$papeleria->Calle:old('Calle') }}" id="Calle">

<br>
<label for="Foto"></label>
@if(isset($papeleria->Foto))
<img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$papeleria->Foto }}" width="100" alt="">
@endif
<input type="file" class="form-control" name="Foto" value="" id="Foto">

<input class="form-control" type="submit" class="btn btn-primary" value="{{$modo}} datos">

<a class="btn btn-outline-info" href="{{ url('papeleria/') }}"> Regresa a tu listado </a>

