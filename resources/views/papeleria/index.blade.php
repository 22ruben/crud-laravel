@extends('layouts.app')

@section('content')
<div class="container">


@if(Session::has('mensaje'))
<div class="alert alert-success" role="alert">
{{ Session::get('mensaje')}}
</div>
@endif


<a href="{{ url('papeleria/create') }}" class="btn btn-outline-info"> Quieres volver a introducir alguna papeleria mas ?</a>
<br>
<br>
<table class="table table-striped">

    <thead class="thead-dark">
        <tr >
            <td>#</td>
            <th> Foto </th>
            <th> Nombre </th>
            <th> Telefono </th>
            <th> Calle </th>
            <th> Botones </th>
        </tr>
    </thead>

    <tbody>
    @foreach($papeleria as $papelerianueva)
        <tr>
            <td>{{$papelerianueva->id}}</td>

            <td>
            <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$papelerianueva->Foto }}" width="100" alt="">
            </td>

            <td>{{$papelerianueva->id }}</td>
            <td>{{$papelerianueva->Nombre }}</td>
            <td>{{$papelerianueva->Telefono }}</td>
            <td>{{$papelerianueva->Calle }}</td>
            <td>

        <a href="{{ url('/papeleria/'.$papelerianueva->id.'/edit') }}" class="btn btn-outline-success">
        Editar
        </a>

            <form method="post" class="d-inline" action="{{ url('/papeleria/'.$papelerianueva->id )}}">
                @csrf
                {{ method_field('DELETE') }}
                <input class="btn btn-danger" type="submit" onclick="return confirm('Quieres borrar? seguro ?')"
                value="Borrar">



            </form>
            
             </td>
        </tr>
        @endforeach
    </tbody>
</table>
{!! $papeleria->links()!!}
</div>
@endsection