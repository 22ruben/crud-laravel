
<h1>FORMULARIO DE PRODUCTOS</h1>

@if(count($errors)>0)

<div class="alert alert-danger" role="alert">
<ul>


@foreach( $errors->all() as $errors)
<li>{{ $errors}}</li>
@endforeach
</ul>
<div>

@endif
<label for="Nombre"> Nombre</label>
<input type="text" name="Nombre" class="form-control is-valid" value="{{ isset($producto->Nombre)?$producto->Nombre:old('Nombre') }}" id="Nombre">


<label for="Tipo"> Tipo</label>
<input type="text" name="Tipo" class="form-control is-valid" value="{{ isset($producto->Tipo)?$producto->Tipo:old('Tipo') }}" id="Tipo">


<label for="Cantidad"> Cantidad</label>
<input type="text" name="Cantidad" class="form-control is-valid"value="{{ isset($producto->Cantidad)?$producto->Cantidad:old('Cantidad') }}" id="Cantidad">

<br>
<label for="Foto"></label>
@if(isset($producto->Foto))
<img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$producto->Foto }}" width="100" alt="">
@endif
<input type="file" class="form-control" name="Foto" value="" id="Foto">

<input class="form-control" type="submit" class="btn btn-primary" value="{{$modo}} datos">

<a class="btn btn-outline-info" href="{{ url('producto/') }}"> Regresa a tu listado </a>

