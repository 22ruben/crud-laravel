@extends('layouts.app')

@section('content')
<div class="container">


@if(Session::has('mensaje'))
<div class="alert alert-success" role="alert">
{{ Session::get('mensaje')}}
</div>
@endif


<a href="{{ url('producto/create') }}" class="btn btn-outline-info"> Quieres volver a introducir algun producto mas ?</a>
<br>
<br>
<table class="table table-striped">

    <thead class="thead-dark">
        <tr>
            <td>#</td>
            <th> Foto </th>
            <th> Nombre </th>
            <th> Tipo </th>
            <th> Cantidad </th>
            <th> Botones </th>
        </tr>
    </thead>

    <tbody>
    @foreach($producto as $productonuevo)
        <tr>
            <td>{{$productonuevo->id}}</td>

            <td>
            <img class="img-thumbnail img-fluid" src="{{ asset('storage').'/'.$productonuevo->Foto }}" width="100" alt="">
            </td>

            <td>{{$productonuevo->id }}</td>
            <td>{{$productonuevo->Nombre }}</td>
            <td>{{$productonuevo->Apellido }}</td>
            <td>{{$productonuevo->Correo }}</td>
            <td>

        <a href="{{ url('/producto/'.$productonuevo->id.'/edit') }}" class="btn btn-outline-success">
        Editar
        </a>

            <form method="post" class="d-inline" action="{{ url('/producto/'.$productonuevo->id )}}">
                @csrf
                {{ method_field('DELETE') }}
                <input class="btn btn-danger" type="submit" onclick="return confirm('Quieres borrar? seguro ?')"
                value="Borrar">



            </form>
            
             </td>
        </tr>
        @endforeach
    </tbody>
</table>
{!! $producto->links()!!}
</div>
@endsection