<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\PapeleriaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

/*Route::get('/usuario', function () {
    return view('usuario.index');
});
*/
//Route::get('/usuario/create', [UsuarioController::class,'create']);

Route::resource('usuario',UsuarioController::class)->middleware('auth');
Auth::routes(['register'=>false,'reset'=>false]);

Route::get('/home', [UsuarioController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', [UsuarioController::class, 'index'])->name('home');
        
    });




    Route::resource('producto',ProductoController::class)->middleware('auth');
    Auth::routes(['register'=>false,'reset'=>false]);


    Route::get('/producto', [ProductoController::class, 'index'])->name('producto');

    Route::prefix(['middleware' => 'auth'], function () {

    Route::get('/', [ProductoController::class, 'index'])->name('producto');
        
    });



    Route::resource('papeleria',PapeleriaController::class)->middleware('auth');
    Auth::routes(['register'=>false,'reset'=>false]);


    Route::get('/papeleria', [PapeleriaController::class, 'index'])->name('papeleria');

    Route::prefix(['middleware' => 'auth'], function () {

    Route::get('/', [PapeleriaController::class, 'index'])->name('papeleria');
        
    });