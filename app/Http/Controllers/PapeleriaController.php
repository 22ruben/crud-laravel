<?php

namespace App\Http\Controllers;

use App\Models\Papeleria;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class PapeleriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['papeleria']=Papeleria::paginate(1);
        return view('papeleria.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('papeleria.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validaciones=[
            'Nombre'=>'required|string|max:120',
            'Telefono'=>'required|string|max:120',
            'Calle'=>'required|string|max:120',
            'Foto'=>'required|max:10000|mimes:jpeg,png,jpg',
        
        ];

        $mensaje=[
            'required'=>'El :attribute tienes que ponerlo',
            'Foto.required'=>'La foto es requerida'
        ];

        $this->validate($request, $validaciones,$mensaje);



        $datosPapeleria = request()->except('_token');

        if($request->hasFile('Foto')){
            $datosPapeleria['Foto']=$request->file('Foto')->store('uploads','public');

        }
        Papeleria::insert($datosPapeleria);
        //return response()->json($datosPapeleria);
        return redirect('papeleria')->with('mensaje','Papeleria agregada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Papeleria  $papeleria
     * @return \Illuminate\Http\Response
     */
    public function show(Papeleria $papeleria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Papeleria  $papeleria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $papeleria=Papeleria::findOrFail($id);
        return view('papeleria.editar', compact('papeleria') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Papeleria  $papeleria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validaciones=[
            'Nombre'=>'required|string|max:120',
            'Telefono'=>'required|string|max:120',
            'Calle'=>'required|integer|max:120',
            'Foto'=>'required|max:10000|mimes:jpeg,png,jpg',
        
        ];

        $mensaje=[
            'required'=>'El :attribute tienes que ponerlo',
            'Foto.required'=>'La foto es requerida'
        ];
        
        if($request->hasFile('Foto')){
            $validaciones=['Foto'=>'required|max:10000|mimes:jpeg,png,jpg',];
            $mensaje=['Foto.required'=>'La foto requerida'];


        }
        $this->validate($request, $validaciones,$mensaje);

        //
        $datosPapeleria = request()->except(['_token','_method']);

        if($request->hasFile('Foto')){
            $papeleria=Papeleria::findOrFail($id);
            Storage::delete('public/'.$papeleria->Foto);

            $datosPapeleria['Foto']=$request->file('Foto')->store('uploads','public');

        }

        Papeleria::where('id','=',$id)->update($datosPapeleria);

        $papeleria=Papeleria::findOrFail($id);
        //return view('papeleria.editar', compact('papeleria') );

        return redirect('papeleria')->with('mensaje','Papeleria modificada con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Papeleria  $papeleria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $papeleria=Papeleria::findOrFail($id);

        if(Storage::delete('public/'.$papeleria->Foto)){

            Papeleria::destroy($id);
        }

        return redirect('papeleria')->with('mensaje','Papeleria borrada');
    }
}
