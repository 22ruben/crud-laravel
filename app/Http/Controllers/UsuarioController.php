<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['usuario']=Usuario::paginate(1);
        return view('usuario.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('usuario.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validaciones=[
            'Nombre'=>'required|string|max:120',
            'Apellido'=>'required|string|max:120',
            'Correo'=>'required|email',
            'Foto'=>'required|max:10000|mimes:jpeg,png,jpg',
        
        ];

        $mensaje=[
            'required'=>'El :attribute tienes que ponerlo',
            'Foto.required'=>'La foto es requerida'
        ];

        $this->validate($request, $validaciones,$mensaje);



        $datosUsuario = request()->except('_token');

        if($request->hasFile('Foto')){
            $datosUsuario['Foto']=$request->file('Foto')->store('uploads','public');

        }
        Usuario::insert($datosUsuario);
        //return response()->json($datosUsuario);
        return redirect('usuario')->with('mensaje','Usuario agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Usuario $usuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usuario=Usuario::findOrFail($id);
        return view('usuario.editar', compact('usuario') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validaciones=[
            'Nombre'=>'required|string|max:120',
            'Apellido'=>'required|string|max:120',
            'Correo'=>'required|email',
            'Foto'=>'required|max:10000|mimes:jpeg,png,jpg',
        
        ];

        $mensaje=[
            'required'=>'El :attribute tienes que ponerlo',
            'Foto.required'=>'La foto es requerida'
        ];
        
        if($request->hasFile('Foto')){
            $validaciones=['Foto'=>'required|max:10000|mimes:jpeg,png,jpg',];
            $mensaje=['Foto.required'=>'La foto requerida'];


        }
        $this->validate($request, $validaciones,$mensaje);

        //
        $datosUsuario = request()->except(['_token','_method']);

        if($request->hasFile('Foto')){
            $usuario=Usuario::findOrFail($id);
            Storage::delete('public/'.$usuario->Foto);

            $datosUsuario['Foto']=$request->file('Foto')->store('uploads','public');

        }

        Usuario::where('id','=',$id)->update($datosUsuario);

        $usuario=Usuario::findOrFail($id);
        //return view('usuario.editar', compact('usuario') );

        return redirect('usuario')->with('mensaje','Usuario modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $usuario=Usuario::findOrFail($id);

        if(Storage::delete('public/'.$usuario->Foto)){

            Usuario::destroy($id);
        }

        return redirect('usuario')->with('mensaje','Usuario borrado');
    }
}
